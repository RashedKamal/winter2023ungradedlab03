public class Application {
    public static void main(String[] args){
    Fox foxFields1 = new Fox();
    foxFields1.name = "Bob";
    foxFields1.age = 10;
    foxFields1.color = "Red";

    System.out.println(foxFields1.name);
    System.out.println(foxFields1.age);
    System.out.println(foxFields1.color);

    Fox foxFields2 = new Fox();
    foxFields2.name = "Billy";
    foxFields2.age = 12;
    foxFields2.color = "White";

    System.out.println(foxFields2.name);
    System.out.println(foxFields2.age);
    System.out.println(foxFields2.color);

    Fox firstFox = new Fox();
    firstFox.sayHi(foxFields1.name);
    firstFox.changeColor(foxFields1.color); 
  
    Fox secondFox = new Fox();
    secondFox.sayHi(foxFields2.name);
    secondFox.changeColor(foxFields2.color);


    Fox[] skulk = new Fox[3];
    skulk[0] = foxFields1;
    skulk[1] = foxFields2;
    skulk[2] = new Fox();
    System.out.println(skulk[2].name);
    }
}